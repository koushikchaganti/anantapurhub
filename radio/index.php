<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Radio AnantapurHub</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="css/styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="main">
  <div id="header">
     <div id="menu">  <a href="http://www.anantapurhub.com">HOME </a>  </div> 
    <div id="header-Bottom">
      <div id="logoBlock">
        <h2><font size="5" color="#fff">AnantapurHub Radio</font></h2>
        <p>Non-Stop Music from Anywhere</p>
      </div>
      <div id="navBlock">
        <h3>Ad Here</h3>
         </div>
    </div>
  </div>
  <div id="mainCont">
    <div id="leftCol">
      <div id="welcomeBox">
        <h3>AnantapurHub Radio Player</h3>
        <p><!-- BEGINS: AUTO-GENERATED MUSES RADIO PLAYER CODE -->
<script type="text/javascript" src="http://hosted.musesradioplayer.com/mrp.js"></script>
<script type="text/javascript">
MRP.insert({
'url':'http://24.191.59.44:8000/live',
'codec':'mp3',
'volume':50,
'autoplay':true,
'buffering':5,
'title':'AnantapurHub',
'welcome':'Anantapur Radio',
'bgcolor':'000000',
'skin':'compact',
'width':191,
'height':46
});
</script>
<!-- ENDS: AUTO-GENERATED MUSES RADIO PLAYER CODE -->
</p>
        
      </div>
      <div id="playListTop">
        <h3>Queued Songs List</h3>
      </div>
      <div id="playListBody">
        <div class="head">
          <p class="left">PLAY</p>
          <p class="centr">TRACK</p>
          <p class="right">ARTIST</p>
        </div>
        <div class="playListDark">
          <p class="play"></p>
          <p class="track">Updated Soon</p>
          <p class="artist">Pending</p>
        </div>
       
      </div>
      <div id="playListBot"></div>
    </div>
    <div id="centrCol">
      <div class="banr"><img src="images/main_banr.jpg" width="275" height="182" border="0" alt="" /></div>
      <div id="albmBlock">
         <div id="albmBox">
          <div class="topCont">
            <h2><!-- <img src="images/lyf.gif" border="0" alt="" /> --></h2>
            <a href="#" class="headings">
			<span>WELCOME TO ANANTAPURHUB RADIO</span></a></div>
          <div class="botCont"> <a class="left" href="#">24/7 Play</a> <a class="right" href="#">Non-Stop</a> </div>
        </div>
       
      </div> 
    </div>
    <div id="rightCol">
      <div id="videoBlock">
        <div id="videoBlockTop">
          <h3>Chat Box</h3>
          <p>This Feature is to interact with AnantapurHub Radio and to meet with new friends</p>
        </div>
        <div id="videoBlockBody">
          <div class="vidBox">
            <div class="leftBox">
              <p class="bold"><b>Online Chat</b></p>
              <p class="light">This Feature Will be Enabled Soon</p>
              <p class="dark">Online Chat will be coming soon</p>
            </div>
            <p class="rightBox"><img src="images/chat.jpg" width="81" height="38" border="0" alt="" /></p>
          </div>
        </div>
      </div>
      <div id="videoBlockBot"></div>
    </div>
  </div>
  <div id="footer">
    <div id="footerMenu"> </div>
    <div class="rights">Copyright � 2013-2014  All Rights Reserved.</div>
    <div class="rights">Designed by: <a href="www.softelitetechnologies.com">SoftElite Technologies</a></div>
  </div>
</div>
</body>
</html>
