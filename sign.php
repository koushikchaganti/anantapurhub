<?php
$refid=@$_GET['refid'];

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>In Ananthapur</title>
<link href="css/style.css" rel="stylesheet"/>
<script src="js/jquery.js" type="text/javascript" ></script>
<script>
$( document ).ready(function() {

   $("#tab li").hover(function() {
		var id= $(this).attr('href');
		$("div[id^=group]").hide();
		$("#" + id).show();
	});
	
    $("#tab li").click(function() {
		var id= $(this).attr('href');
		$("div[id^=group]").hide();
		var again = $("#" + id).fadeIn();
		$("#tab li").unbind("mouseenter mouseleave");
	});
   
});
</script>
</head>

<body>
	
	
	
	<header>
		<div class="container">	
			<img src="img/logo.png" alt="logo"/>
			<p><span style="font-weight:bold; font-size:22px;">Yours's Largest Marketplace</span><br/>
				<span style="font-size:12px;"> Online Classified Advertising India's Largest Marketplace</span>
			</p>
			<ul style="list-style:none;">
				<li><a style="background:#001f5f; color:#fff;border-radius:4px; border:none; margin-left:2px" href="index.php">Home</a></li>
				<li><a style=" background:#ff0000; color:#fff; border-radius:4px; border:none; margin-left:4px; border-right:0px;" href="log.php">Login</a></li>
			</ul>
				
		</div>
		</div>
	</header>

	<section>
	
<div class="container">
				
	<div style="width:73%" class="bg">	
			<p style="margin:15px; padding-bottom:10px; border-bottom:1px solid #000; font-size:16px; font-weight:bold; color:#000;">Signup Form:</p>
         <?php
		 $link="signup.php?refid=".$refid.""
		 ?>		                    

              <form method="post" name="signup" action="<?php echo $link;?>">
				
				<?php
                       //echo "<center><span style=\"color:red\">$success</span></center>";


                       if (isset($_POST['submit']) || @$_POST['submit']==true)
					   {
					   echo "<center>$error</center>";
					   }
                     
				       /*echo "<center><font size='3' color='red'>* All fields are required</font></center> ";*/
				 ?>

				  

				<table style="float: left;">
				
				<tr>
					<td><strong><font color="red">* </font>First Name&nbsp;:</strong></td>
					<td><input type="text" name="firstname"></td>
				</tr>
				<tr>
					<td colspan="2"><input type="hidden" name="refid" value="<?php echo $refid;?>"></td>
				</tr>
				<tr>
					<td><strong><font color="red">* </font>Last Name&nbsp;:</strong></td>
					<td><input type="text" name="lastname"></td>
				</tr>
				<tr>
					<td align="center" colspan="2" id="lastname"></td>
				</tr>
				<tr>
				    <td><strong><font color="red">* </font>I&nbsp;Am&nbsp;:</strong></td>
					<td><select name="iam">
						<option value="" selected></option>
						<option value="Individual">Individual</option>
						<option value="A Professional/Bussiness">A Professional/Bussiness</option>
						
					</select></td>
				</tr>
				<tr>
				    <td><strong><font color="red">* </font>Username&nbsp;:</strong></td>
					<td><input type="text" name="username"></td>
				</tr>
				<tr>
					<td align="center" colspan="2" id="username"></td>
				</tr>
				
				<tr>
					<td align="center" colspan="2">
					
					
					
					
					</td>
				</tr>
				<tr>
				    <td><strong><font color="red">* </font>Password&nbsp;:</strong></td>
					<td><input type="password" name="password" placeholder="Must Contain 6-8 Charecters" maxlength="8"></td>
				</tr>
				<tr>
					<td align="center" colspan="2" id="pwd1"></td>
				</tr>
				<tr>
				    <td><strong><font color="red">* </font>Confirm&nbsp;Password&nbsp;:</strong></td>
					<td><input type="password" name="password2" placeholder="Must Contain 6-8 Charecters" maxlength="8"></td>
				</tr>
				<tr>
					<td align="center" colspan="2" id="pwd2"></td>
				</tr>

				<tr>
					<td><strong><font color="red">* </font>DOB&nbsp;:</strong></td>
					<td>
					
					<!-- Date -->

					<select name="date">
						<option value="<?php echo $date;?>" selected>
						<option value="01">01</option>
						<option value="02">02</option>
						<option value="03">03</option>
						<option value="04">04</option>
						<option value="05">05</option>
						<option value="06">06</option>
						<option value="07">07</option>
						<option value="08">08</option>
						<option value="09">09</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>
						<option value="24">24</option>
						<option value="25">25</option>
						<option value="26">26</option>
						<option value="27">27</option>
						<option value="28">28</option>
						<option value="29">29</option>
						<option value="30">30</option>
						<option value="31">31</option>
					</select>

					<!-- Month -->
             
			       <select name="month">
					<option value="" selected>
					<option value="January">JAN</option>
					<option value="February">FEB</option>
					<option value="March">MAR</option>
					<option value="April">APR</option>
					<option value="May">MAY</option>
					<option value="June">JUN</option>
					
					<option value="Jul">JUL</option>
					<option value="Aug">AUG</option>
					<option value="Sep">SEP</option>
					<option value="Oct">OCT</option>
					<option value="Nov">NOV</option>
					<option value="Dec">DEC</option>
			       </select>

				   <!-- Year -->

				   <select name="year">
					<option value="" selected>
					
					<option value="1961">1961</option>
					<option value="1962">1962</option>
					<option value="1963">1963</option>
					<option value="1964">1964</option>
					<option value="1965">1965</option>
					<option value="1966">1966</option>
					<option value="1967">1967</option>
					<option value="1968">1968</option>
					<option value="1969">1969</option>
					<option value="1970">1970</option>
					<option value="1971">1971</option>
					<option value="1972">1972</option>
					<option value="1973">1973</option>
					<option value="1974">1974</option>
					<option value="1975">1975</option>
					<option value="1976">1976</option>
					<option value="1977">1977</option>
					<option value="1978">1978</option>
					<option value="1979">1979</option>
					<option value="1980">1980</option>
					<option value="1981">1981</option>
					<option value="1982">1982</option>
					<option value="1983">1983</option>
					<option value="1984">1984</option>
					<option value="1985">1985</option>
					<option value="1986">1986</option>
					<option value="1987">1987</option>
					<option value="1988">1988</option>
					<option value="1989">1989</option>
					<option value="1990">1990</option>
					<option value="1991">1991</option>
					<option value="1992">1992</option>
					<option value="1993">1993</option>
					<option value="1994">1994</option>
					<option value="1995">1995</option>
					<option value="1996">1996</option>
					

				   </select>

					
					</td>
				</tr>
				<tr>
					<td align="center" colspan="2" id="dob"></td>
				</tr>
				<tr>
					<td><strong><font color="red">* </font>Sex&nbsp;:</strong></td>
					<td><input type="radio" id="sex" name="sex" value="Male">Male&nbsp;<input type="radio" id="sex" name="sex" value="Female">Female</td>
				</tr>
				<tr>
					<td align="center" colspan="2" id="sex1"></td>
				</tr>
				<tr>
					<td><strong><font color="red">* </font>Email&nbsp;:</strong></td>
					<td><input type="text" name="email"></td>
				</tr>
				<tr>
					<td align="center" colspan="2" id="email"></td>
				</tr>
				<tr>
					<td align="center" colspan="2">
					
					
					
					
					</td>
				</tr>
				<tr>
					<td><strong><font color="red">* </font>Mobile&nbsp;:</strong></td>
					<td><strong>+91-</strong><input type="text" name="mobile" size="15" maxlength="10"></td>
				</tr>
				<tr>
					<td align="center" colspan="2" id="mobile"></td>
				</tr>
				<tr>
					<td align="center" colspan="2">
					
					<!--<?php 
					if($mobile!="")
					{
						echo $mobileerror;
					}
					?>-->
					
					
					</td>
				</tr>
				
				<tr>
					<td><strong><font color="red">* </font>Occupation&nbsp;:</strong></td>
					<td><select name="occupation">
						<option value="" selected>
						<option value="Employee">Employee</option>
						<option value="Student">Student</option>
						<option value="Other">Other</option>
					</select></td>
				</tr>
				</table>
				<table style="float:left; padding-left:40px;">
				<tr>
					<td align="center" colspan="2" id="occupation"></td>
				</tr>
				<tr>
					<td><strong><font color="red">* </font>Address&nbsp;:</strong></td>
					<td><textarea name="address" rows="5" cols="20" placeholder="Address length shouldn't be less than 15 charecters"></textarea></td>
				</tr>
				<tr>
					<td align="center" colspan="2" id="address"></td>
				</tr>
				<tr>
					<td><strong><font color="red">* </font>Country&nbsp;:</strong></td>
					<td><select  name="country" id="country" onChange="return states(this.value)">
						<option value="" selected [Choose Yours]></option>
						<option value="INDIA">INDIA</option>
						
   					</select></td>
				</tr>
				
				
				<tr>
					<td align="center" colspan="2" id="country1"></td>
				</tr>
				<tr>
				<td><strong><font color="red">* </font>State&nbsp;:</strong></td>
                <td><select name="states" >
					     <option value='' selected>
					     <option value='Andhra Pradesh'>Andhra Pradesh</option>
					     
					     <option value=''></option>
					    
					
                </select></td> 
				<!--<script>
					  function states(country)
					  {
						  var stateselect="";
						  if(country=="INDIA" && country!="OTHER")
						  {
					   stateselect="<td><select name='states' id='states'>"
					   +"<option value='' selected>"
					   +"<option value='Andhra Pradesh'>Andhra Pradesh</option>"
					   +"<option value='Arunachal Pradesh'>Arunachal Pradesh</option>"
					   +"<option value=''></option>"
					   +"</select></td>";
					  
						  }
						  else
						  {

						     stateselect="<td><strong>NOTE&nbsp;:</strong></td>"
							 +"<td><font color='red'>Sorry Only <b>"
							 +"<font color='green'>INDIAN'S</font></b>Join In This Site.</font></td>";
						  }
						  document.getElementById("states").innerHTML=stateselect;
						 

					  }
					</script>-->
				</tr>
				<tr>
					<td align="center" colspan="2" id="state1"></td>
				</tr>
				<tr>
					<td><strong><font color="red">* </font>City&nbsp;:</strong></td>
					<td><input type="text" name="city"></td>
				</tr>
				<tr>
					<td align="center" colspan="2" id="city"></td>
				</tr>
				<tr>
					<td><strong><font color="red">* </font>Pincode&nbsp;:</strong></td>
					<td><input type="text" name="pincode" maxlength="6"></td>
				</tr>
				<tr>
					<td align="center" colspan="2" id="pincode"></td>
				</tr>
		<!--		<tr>
					<td><strong>Verification Code&nbsp;:</strong></td>
					<td><br/><input type="text" name="verification" value=<?php echo "$verify";?> disabled><br/><input type="text" name="verificationcode"></td>
				</tr>-->
				<tr>
					<td align="right"><input type="submit" name="submit" value="submit"></td>
					<td><input type="reset"></td>
				</tr>

				</table>
              </form>

          

		
	</div>
		
		
		<div style="margin-left:10px;" class="adv">
			<img style="margin-top:0px;" src="Ads/adv3.jpg" alt="adv">
                       <img style="margin-top:0px;" src="Ads/adv3.jpg" alt="adv">
		</div>		
		
	
	</div>
		</section>
		
	<footer>
		<p>Copyright &copy; 2014 Softelite technologies.</p>
	</footer>
	
</body>
</html>
